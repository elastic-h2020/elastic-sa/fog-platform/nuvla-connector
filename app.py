import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from shutil import copy2, SameFileError
from os import getenv

# Shared volume
# DATA_VOLUME = "/srv/nuvlabox/shared"
DATA_VOLUME = getenv('DATA_VOLUME', '.')
# CONTAINER_VOLUME_PATH = "/konnektbox-security/alerts/nuvla/"
CONTAINER_VOLUME_PATH = getenv('CONTAINER_VOLUME_PATH', './test_folder')


def on_created(event):
    print(f"hey, {event.src_path} has been created!")


def on_deleted(event):
    print(f"what the f**k! Someone deleted {event.src_path}!")


def on_modified(event):
    print(f"hey, {event.src_path} has been modified")
    try:
        copy2(event.src_path, CONTAINER_VOLUME_PATH)
    except SameFileError:
        pass


def on_moved(event):
    print(f"ok ok ok, someone moved {event.src_path} to {event.dest_path}")


if __name__ == "__main__":

    # BASIC LOGGING
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    path = sys.argv[1] if len(sys.argv) > 1 else DATA_VOLUME

    print("[?] Monitoring file ->" + DATA_VOLUME + "\n")

    # Pattern matching
    patterns = ["vulnerabilities", "vulnerabilities*"]
    ignore_patterns = ["*~"]
    ignore_directories = False
    case_sensitive = True
    event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)

    event_handler.on_deleted = on_deleted
    event_handler.on_modified = on_modified
    event_handler.on_moved = on_moved
    #event_handler.on_created = on_created

    go_recursively = False
    observer_pattern = Observer()
    observer_pattern.schedule(event_handler, path, recursive=go_recursively)
    observer_pattern.start()

    try:
        while True:
            time.sleep(1)
    finally:
        observer_pattern.stop()
        observer_pattern.join()
