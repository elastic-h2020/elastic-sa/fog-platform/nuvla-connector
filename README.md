# Wazuh Manager configuration

## In the Manager's /var/ossec/etc/shared/agent.conf

```xml
<agent_config profile="hardening-W7">
  <localfile>
    <location>FARR.SP</location>
    <log_format>eventchannel</log_format>
  </localfile>
  <localfile>
    <log_format>json</log_format>
    <location>/konnektbox-security/alerts/nuvla/vulnerabilities</location>
  </localfile>
</agent_config>
```

# Nuvla's vulnerabily file

/srv/nuvlabox/shared/vulnerabilities
