FROM python:3.7-alpine

WORKDIR /nuvlabox-connector
ENV PYTHONUNBUFFERED=1

RUN mkdir ./alerts
RUN apk add --no-cache --update gcc musl-dev linux-headers
COPY requirements.txt ./
RUN python3 -m pip install -r requirements.txt
COPY . .

VOLUME /srv/nuvlabox/shared

ENTRYPOINT [ "python3", "app.py"]